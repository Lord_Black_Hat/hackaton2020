import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import CandidateSign from "../../components/CandidateSign/index";
import "./index.css";

const Elections = () => {
  const history = useHistory();

  const [data, setData] = useState({});

  const getData = () => {
    const token = localStorage.getItem("token");
    const body = {
      'token':token
    }
    fetch("http://127.0.0.1:5000/candidates", {
      method:'POST',
      body: JSON.stringify(body)
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setData(data.candidates);
      });
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div className="pageFiller">
      <div id="electionsRootContainer">
        <div id="electionCurrentCandidates"> Текущие кандидаты:</div>
        {data.length > 0 ? (
          data.map((person, index) => {
            console.log(person)
            return (
              <CandidateSign
                {...{ name: person[1], description: person[2] }}
                onClick={() => {
                  history.push("/profile:" + person[0]);
                }}
              />
            );
          })
        ) : (
          <div>Данные загружаются...</div>
        )}
      </div>
    </div>
  );
};

export default Elections;
