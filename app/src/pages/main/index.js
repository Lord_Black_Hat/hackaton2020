import React from "react";
import { useHistory } from "react-router-dom";
import "./index.css";

const Main = () => {
  const history = useHistory();

  return (
    <div className="pageFiller">
      <div className="mainRootContainer">
        <div className="basicText">Главная страница выборов Школы 444</div>
        <br></br>
        <button
          onClick={() => {
            history.push("/elections");
          }}
        >
          Выборы
        </button>
        <button
          onClick={() => {
            localStorage.setItem("token", "");
            window.location.reload();
          }}
        >
          Сбросить авторизацию!
        </button>
      </div>
    </div>
  );
};

export default Main;
