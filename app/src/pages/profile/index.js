import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import "./index.css";
const Profile = ({ ...props }) => {
  const [data, setData] = useState({});

  let { id } = useParams();
  id = id.slice(1);

  const getData = () => {
const token = localStorage.getItem("token");
const body = {'token': token}
    fetch("http://127.0.0.1:5000/profile/" + id, {
      method:'POST',
      body: JSON.stringify(body)
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setData(data);
      });
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div className="pageFiller">
      <div id="profileRootContainer">
        {data.length > 1 ? (
          <div>
            <div id="profileName">{data[1]}</div>
            <div id="profileBio">{data[2]}</div>
          </div>
        ) : (
          <div>Данные загружаются...</div>
        )}
      </div>
    </div>
  );
};

export default Profile;
