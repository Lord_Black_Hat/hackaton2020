import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import LoginForm from "../../components/LoginForm";
import "./index.css";

const Register = () => {
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [key, setSecretKey] = useState("");

  const history = useHistory();
  if (localStorage.getItem("token")) history.push("/");
  const body = {
    'username': login,
    'password': password,
    'secretKey': key,
  };

  const register = () => {
    fetch("http://127.0.0.1:5000/register", {
      method: "POST",
      body: JSON.stringify(body),
    }).then(res=>res.json()).then(
      data=> {
        console.log(data)
        if (data.result == 'true') {
          history.push('/login')
        }
      }
    );
  };

  return (
    <div className="pageFiller">
      <div className="loginRootContainer">
        <div id="authWarningText">Сначала вам необходимо авторизоваться:</div>
        <div id="authFormContainer">
          <LoginForm
            placeholder="Логин"
            onChange={(event) => setLogin(event.target.value)}
            className="authForm"
          />

          <LoginForm
            placeholder="Пароль"
            onChange={(event) => setPassword(event.target.value)}
            className="authForm"
          />

          <LoginForm
            placeholder="Секретный ключ"
            onChange={(event) => setSecretKey(event.target.value)}
            className="authForm"
          />
          <button
            onClick={register}
          >
            Подтвердить
          </button>
        </div>
      </div>
    </div>
  );
};

export default Register;
