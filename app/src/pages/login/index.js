import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import LoginForm from "../../components/LoginForm";
import "./index.css";

const Login = () => {
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [key, setSecretKey] = useState("");

  const history = useHistory();
  if (localStorage.getItem("token")) history.push("/");

  const checkAuthorization = () => {
    const body = {
      password: password,
      username: login,
    };
    fetch("http://127.0.0.1:5000/login", {
      method: "POST",
      body: JSON.stringify(body),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.token) {
          localStorage.setItem("token", data.token);
          window.location.reload();
        }
      });
  };

  return (
    <div className="pageFiller">
      <div className="loginRootContainer">
        <div id="authWarningText">Сначала вам необходимо авторизоваться:</div>
        <div id="authFormContainer">
          <LoginForm
            placeholder="Логин"
            onChange={(event) => setLogin(event.target.value)}
            className="authForm"
          />
          <LoginForm
            placeholder="Пароль"
            onChange={(event) => setPassword(event.target.value)}
            className="authForm"
          />
          <div
            id="noAccountHref"
            onClick={() => {
              history.push("/register");
            }}
          >
            Нет аккаунта?
          </div>
          <button
            onClick={() => {
              checkAuthorization();
            }}
          >
            Подтвердить
          </button>
        </div>
      </div>
    </div>
  );
};

export default Login;
