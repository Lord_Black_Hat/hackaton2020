import logo from "./logo.svg";
import React, { useState, useEffect } from "react";
import { HashRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./pages/main";
import Elections from "./pages/elections";
import Login from "./pages/login";
import Profile from "./pages/profile";
import Register from "./pages/register";
import PrivateRoute from "./components/PrivateRoute";

function App() {
  const [isAuthenticated, setAuthentication] = useState(null);

  const getAuthenticate = () => {
    const token = localStorage.getItem("token");
    console.log({ token });
    fetch("http://127.0.0.1:5000/checkAuthorization", {
      method: "POST",
      body: JSON.stringify({
        token: token,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data.result == "true") {
          setAuthentication(true);
        } else {
          setAuthentication(false);
        }
      });

    // const token = localStorage.getItem("token");
    // setAuthentication(token)
  };

  useEffect(() => {
    getAuthenticate();
  }, []);

  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/login">
          <Login />
        </Route>
        <Route exacp path="/register">
          <Register />
        </Route>
        <PrivateRoute exact path="/profile:id" {...{ isAuthenticated }}>
          <Profile />
        </PrivateRoute>
        <PrivateRoute exact path="/elections" {...{ isAuthenticated }}>
          <Elections />
        </PrivateRoute>
      </Switch>
    </Router>
  );
}

export default App;
