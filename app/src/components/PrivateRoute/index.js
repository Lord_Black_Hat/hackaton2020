import React, { useState } from "react";

import { Route, Redirect } from "react-router-dom";

const PrivateRoute = ({ isAuthenticated, children, ...rest }) => {
  return (
    <Route
      {...rest}
      render={() => {
        return isAuthenticated ? children : <Redirect to="/login" />;
      }}
    />
  );
};

export default PrivateRoute;
