import React from "react";
import "./index.css";

const CandidateSign = ({ name, description, onClick, ...props }) => {
  return (
    <div {...props} id="candidateSignContainer">
      <div onClick={onClick} id="candidateName">
        {name}
      </div>
      <div id="candidateDescription">{description}</div>
    </div>
  );
};

export default CandidateSign;
