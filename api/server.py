from flask import Flask, request, jsonify, make_response
from flask_cors import CORS
import sqlite3
import json
import hashlib
import datetime
import random
import string

app = Flask(__name__)
CORS(app)

def create_profile(uid, classId, name='', bio=''):
    db = sqlite3.connect('main.db')
    cursor = db.cursor()
    values = (uid, classId, name, bio)
    query = 'INSERT INTO Profiles (uid, classId, name, bio) values(?, ?, ?, ?)'
    cursor.execute(query, values)
    db.commit()
    db.close()
    return True



def execute_db(query, values):
    db = sqlite3.connect('main.db')
    cursor = db.cursor()
    cursor.execute(query, values)
    db.commit()
    db.close()
    return True



def query_one_db(query, values):
    db = sqlite3.connect('main.db')
    cursor = db.cursor()
    cursor.execute(query, values)
    result = cursor.fetchone()
    db.close()
    return result

def query_all_db(query, values):
    db = sqlite3.connect('main.db')
    cursor = db.cursor()
    cursor.execute(query, values)
    result = cursor.fetchall()
    db.close()
    return result


def check_token_in_data(data):
    if ('token' not in data):
        return False
    if (not check_session(data['token'])):
        return False

    return True


def get_random_string(length):
    letters = string.ascii_letters
    result_string = ''.join([random.choice(letters) for i in range(length)])
    return result_string



def check_session(token):
    values = (token, )
    query = 'SELECT * FROM Sessions WHERE token=?'
    user = query_one_db(query, values)
    if (user is not None):
        return True
    else:
        return False



def create_session(uid):
    token = ''
    current_date = datetime.datetime.now()

    db = sqlite3.connect('main.db')
    cursor = db.cursor()
    token = get_random_string(32)

    execute_query = 'INSERT INTO Sessions (token, uid, timeCreated) values(?, ?, ?)'

    values = (token, uid, str(current_date))

    cursor.execute(execute_query, values)
    db.commit()

    return token


def getAuthId(token):
    values = (token, )
    query = 'SELECT * FROM Sessions WHERE token=?'
    user = query_one_db(query, values)

    uid = user[1]
    return uid




def check_secret_key(key):
    values = (key, )
    query = 'SELECT * FROM SecretKeys WHERE key=?'
    user = query_one_db(query, values)

    if (user is not None):
        if (user[1] != 'True'):
                db = sqlite3.connect('main.db')
                cursor = db.cursor()
                values = (key, )
                query = "UPDATE SecretKeys SET isUsed='True' WHERE key=?"
                cursor.execute(query, values)
                db.commit()
                db.close()
                classId = query_one_db("SELECT classId FROM SecretKeys WHERE key=?", (key, ))[0]
                return classId
    return 'False'



#Authorization here

@app.route('/login', methods=['POST'])
def login():
    data = request.get_data()
    data = json.loads(data)
    print(data)
    if ('username' not in data or 'password' not in data):
        return {'error':'No name or password'}
    else:
        # some logic here        
        md5 = hashlib.md5(data['password'].encode()).hexdigest()
        values = (data['username'], md5)
        query = 'SELECT * FROM Users WHERE uid=? AND password=?'
        user = query_one_db(query, values)

        
        if (user is not None):
            result = {'token': create_session(data['username'])}
            return (result, {'Access-Control-Allow-Origin': '*'})
        else:
            result = {'error':'user not found'}
            return (result, {'Access-Control-Allow-Origin': '*'})









@app.route('/register', methods=['POST'])
def register():
    data = request.get_data()
    data = json.loads(data)
    print(data)
    if ('username' not in data or 'password' not in data or 'secretKey' not in data):
        result =  {'error':'No username or password or secretKey'}
        return (result, {'Access-Control-Allow-Origin': '*'})
    query = 'SELECT * FROM Users WHERE uid=?'
    values = (data['username'], )
    if (query_one_db(query, values) is not None):
        result =  {'error':'This user already exists'}
        return (result, {'Access-Control-Allow-Origin': '*'})
    else:
        classId = check_secret_key(data['secretKey'])
        if (classId != 'False'):



            db = sqlite3.connect('main.db')
            cursor = db.cursor()
            md5 = hashlib.md5(data['password'].encode()).hexdigest()
            execute_query = 'INSERT INTO Users (uid, password) values(?, ?)'
            values = (data['username'], md5)

            cursor.execute(execute_query, values)
            db.commit()
            db.close()


            create_profile(data['username'], classId)



            result = {'result':'true'}
            return (result, {'Access-Control-Allow-Origin': '*'})
        else:
            result = {'result':'false'}
            return (result, {'Access-Control-Allow-Origin': '*'})







@app.route('/editProfile', methods=['POST'])
def editProfile():
    data = request.get_data()
    data = json.loads(data)

    if (not check_token_in_data):
        return ({'result': None}, {'Access-Control-Allow-Origin': '*'})
    uid = getAuthId(data['token'])

    if ( 'bio'not in data or  'name' not in data):
        return ({'result': 'False'}, {'Access-Control-Allow-Origin': '*'})
    
    query = "UPDATE Profiles SET name=?, bio=? WHERE uid=?"
    values = (data['name'], data['bio'], uid, )
    execute_db(query, values)

    return ({'result': 'True'}, {'Access-Control-Allow-Origin': '*'})





@app.route('/voteParlament', methods=['POST'])
def vote_parlament():
    data = request.get_data()
    data = json.loads(data)
    if (not check_token_in_data):
        return ({'result': 'false'}, {{'Access-Control-Allow-Origin': '*'}})
    if ('uid' not in data or 'electionsId' not in data):
        return ({'result': 'false'}, {{'Access-Control-Allow-Origin': '*'}})

    
    




@app.route('/candidates', methods=['POST'])
def get_candidates():
    data = request.get_data()
    data = json.loads(data)
    if (not check_token_in_data):
        return ({'result': None}, {{'Access-Control-Allow-Origin': '*'}})

    uid = getAuthId(data['token'])

    query = "SELECT classId FROM Profiles WHERE uid=?"
    values=(uid, )
    classId = query_one_db(query, values)[0]

    query = "SELECT electionsId FROM ParlamentElections WHERE classId=? AND isClosed='False'"
    values= (classId,)
    electionsId = query_one_db(query, values)[0]


    query = "SELECT uid FROM ParlamentCandidates WHERE electionsId = ?"
    values = (electionsId, )
    candidateUids = query_all_db(query, values)
    
    candidateProfiles = []
    
    for uid_tuple in candidateUids:
        print(uid_tuple)
        query = "SELECT * FROM Profiles WHERE uid = ?"
        values = uid_tuple
        candidateProfiles.append(query_one_db(query, values))

    result = {'electionsId': electionsId, 'candidates':candidateProfiles}
    result = json.dumps(result)
    return (result, {'Access-Control-Allow-Origin': '*'})



@app.route('/checkAuthorization', methods=['POST'])
def checkAuthorization():
    data = request.get_data()
    data = json.loads(data)

    if (check_token_in_data(data)):
        result = {'result':'true'}
        return (result, {'Access-Control-Allow-Origin': '*'})
    else:
        result = {'result':'false'}
        return (result, {'Access-Control-Allow-Origin': '*'})




@app.route('/profile/<uid>', methods=['POST'])
def getProfile(uid):
    data = request.get_data()
    data = json.loads(data)

    if (not check_token_in_data(data)):
        return ({'result': None}, {{'Access-Control-Allow-Origin': '*'}})

    db = sqlite3.connect('main.db')
    print(uid)
    query = 'SELECT * FROM Profiles WHERE uid=?'
    values =  (uid, )
    user =query_one_db(query, values)
    result = json.dumps(user)
    return (result, {'Access-Control-Allow-Origin': '*'})


if __name__ == '__main__':
    app.run(debug=True,)
